import React, { useState } from "react";

import { When } from "react-if";

import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Alert from "react-bootstrap/Alert";
import Spinner from "react-bootstrap/Spinner";

function Request() {
  const [submitting, setSubmitting] = useState(false);
  const [success, setSuccess] = useState(false);

  const handleSubmit = async (event) => {
    setSubmitting(true);
    event.preventDefault();
    const { target } = event;
    const formData = new FormData(target);

    try {
      await fetch("/", {
        method: "POST",
        body: formData,
      });
      setSubmitting(false);
      target.reset();
      setSuccess(true);
    } catch (e) {
      console.log(e);
      setSubmitting(false);
    }
  };

  return (
    <>
      <When condition={submitting}>
        <Alert variant="info" className="text-center">
          <Spinner animation="grow" role="status" />
          Submitting...
          <Spinner animation="grow" role="status" />
        </Alert>
      </When>
      <When condition={success}>
        <Alert variant="success" className="text-center">
          Request submitted successfully
        </Alert>
      </When>
      <Form data-netlify="true" onSubmit={handleSubmit}>
        <input type="hidden" name="form-name" value="request" />
        <Form.Group as={Row} controlId="formHorizontalBotField" hidden>
          <Form.Label column sm={2}>
            Don’t fill this out if you’re human:
          </Form.Label>
          <Col sm={6}>
            <Form.Control name="bot-field" />
          </Col>
        </Form.Group>
        <Row>
          <Col>
            <Card>
              <Card.Header>Metadata</Card.Header>
              <Card.Body>
                <div className="spacer mt-3" />
                <Form.Group as={Row} controlId="formHorizontalEmail">
                  <Form.Label column sm={3}>
                    Email
                  </Form.Label>
                  <Col sm={5}>
                    <Form.Control required name="email" type="email" />
                  </Col>
                  <Col sm={4}>
                    <Form.Text id="emailHelpBlock">
                      Validated using{" "}
                      <a href="https://emailrep.io/">https://emailrep.io/</a>
                    </Form.Text>
                  </Col>
                </Form.Group>
                <Form.Group as={Row} controlId="formGridNotes">
                  <Form.Label column sm={3}>
                    Notes
                  </Form.Label>
                  <Col sm={5}>
                    <Form.Control
                      as="textarea"
                      autoCorrect="on"
                      rows="3"
                      name="notes"
                    />
                  </Col>
                  <Col sm={4}>
                    <Form.Text id="notesHelpBlock">
                      Anything else: words of appreciation, details of
                      pre-arranged requests, feature suggestions, etc.
                    </Form.Text>
                  </Col>
                </Form.Group>
                <Form.Group as={Row} controlId="formHorizontalAdvanced">
                  <Form.Label column sm={3}>
                    Advanced pack
                  </Form.Label>
                  <Col sm={5}>
                    <Form.Switch name="advanced" />
                  </Col>
                  <Col sm={4}>
                    <Form.Text id="advancedHelpBlock">
                      More sophisticated cards
                    </Form.Text>
                  </Col>
                </Form.Group>
              </Card.Body>
            </Card>
          </Col>
          <Col md={12} lg={6}>
            <Card>
              <Card.Header>Destination</Card.Header>
              <Card.Body>
                <Form.Group as={Row} controlId="formHorizontalName">
                  <Form.Label required column sm={2}>
                    Name
                  </Form.Label>
                  <Col sm={6}>
                    <Form.Control
                      name="name"
                      type="text"
                      placeholder="Name for address label"
                    />
                  </Col>
                </Form.Group>

                <Form.Group controlId="formGridAddress1">
                  <Form.Label>Address</Form.Label>
                  <Form.Control
                    required
                    type="text"
                    name="address1"
                    placeholder="1234 Main St"
                  />
                </Form.Group>

                <Form.Group controlId="formGridAddress2">
                  <Form.Label>Address 2</Form.Label>
                  <Form.Control
                    name="address2"
                    type="text"
                    placeholder="Apartment, studio, or floor"
                  />
                </Form.Group>

                <Form.Row>
                  <Form.Group as={Col} md="4" controlId="formGridCity">
                    <Form.Label>City</Form.Label>
                    <Form.Control required type="text" name="city" />
                  </Form.Group>

                  <Form.Group as={Col} md="5" controlId="formGridRegion">
                    <Form.Label>State / Province / Region</Form.Label>
                    <Form.Control type="text" name="region" />
                  </Form.Group>

                  <Form.Group as={Col} md="3" controlId="formGridPostal">
                    <Form.Label>Postal Code</Form.Label>
                    <Form.Control required type="text" name="postalcode" />
                  </Form.Group>
                </Form.Row>

                <Form.Group as={Row} controlId="formGridCountry">
                  <Form.Label column sm={3}>
                    Country
                  </Form.Label>
                  <Col sm={5}>
                    <Form.Control type="text" name="country" />
                  </Col>
                </Form.Group>
              </Card.Body>
            </Card>
          </Col>
        </Row>
        <Row className="pt-5">
          <Col className="text-center justify-content-center">
            <Button variant="success" disabled={submitting} type="submit">
              Request
            </Button>
          </Col>
        </Row>
      </Form>
    </>
  );
}

export default Request;
