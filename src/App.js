import React from 'react';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavLink from 'react-bootstrap/NavLink';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

import Footer from './Footer';
import Request from './Request';

function App() {
  return (
    <>
      <Navbar collapseOnSelect expand="sm" bg="light" variant="light">
        <Navbar.Brand as={NavLink} to="/">
          Sampler
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav>
            <Navbar.Text>
            </Navbar.Text>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <Container fluid="md" className="mt-3">
        <Row>
          <Col>
            <Request />
          </Col>
        </Row>
      </Container>
      <Footer />
    </>
  );
}

export default App;
