import React from 'react';

import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

function Footer(props) {
  return (
    <>
      <footer className="page-footer font-small mt-5">
        <Container fluid className="text-center">
          <Row noGutters>
            <Col>
              <small className="text-muted">
              </small>
            </Col>
            <Col>
            </Col>
            <Col>
              <small className="text-muted">
              </small>
            </Col>
          </Row>
        </Container>
      </footer>
    </>
  );
}

export default Footer;
