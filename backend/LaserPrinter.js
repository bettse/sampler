require("dotenv").config();

const util = require("util");
const fs = require("fs");
const ipp = require("ipp");
const PDFDocument = require("pdfkit");

const Printer = require("./Printer");

const { LASER_PRINTER_URI, RETURN_ADDRESS, NODE_ENV } = process.env;

//PostScript points.
const letter = {
  height: 792,
  width: 612,
};

// The envelope is larger than the folder paper, so this compensates for the paper not lining up with the top of the envelope
const envelopeAdjustment = 24;

const returnWindow = {
  top: 45 - envelopeAdjustment,
  left: 36,
  width: 252,
  height: 72,
  outline: false,
};

const addressWindow = {
  top: 162 - envelopeAdjustment,
  left: 36,
  width: 288,
  height: 81,
  outline: false,
};

//landscape
const badgePt = {
  height: 153,
  width: 243,
};

const returnAddressParts = RETURN_ADDRESS.split("|");

class LaserPrinter extends Printer {
  constructor() {
    super();
    const laserPrinter = ipp.Printer(LASER_PRINTER_URI);
    this.print = util.promisify(laserPrinter.execute.bind(laserPrinter));
  }

  async composePdf(request) {
    const {
      name,
      address1,
      address2,
      city,
      region,
      postalcode,
      country,
      notes,
      advanced,
    } = request;
    const size = "LETTER";
    const margin = {
      left: 5,
      top: 5,
    };

    return new Promise((resolve, reject) => {
      const doc = new PDFDocument({ size });

      let buffers = [];
      doc.on("error", reject); //dunno if it actually emits this
      doc.on("data", buffers.push.bind(buffers));
      doc.on("end", () => resolve(Buffer.concat(buffers)));

      if (NODE_ENV === "development") {
        doc.pipe(fs.createWriteStream("./letter.pdf")); // write to PDF
      }

      // Fold marks
      const leftSide = 10;
      const foldMarkWidth = 50;
      const firstMark = letter.height * (1 / 3);
      const secondMark = letter.height * (2 / 3);

      [firstMark, secondMark].forEach((mark) => {
        //left
        doc
          .moveTo(leftSide, mark)
          .lineTo(foldMarkWidth, mark)
          .dash(5, { space: 10 })
          .stroke();

        //right
        doc
          .moveTo(letter.width, mark)
          .lineTo(letter.width - foldMarkWidth, mark)
          .dash(5, { space: 10 })
          .stroke();
      });

      // Left and right
      const xl = letter.width * (1 / 4) - badgePt.width / 2;
      const xr = letter.width * (3 / 4) - badgePt.width / 2;
      // High and low
      const yh = letter.height * (1 / 6) - badgePt.height / 2;
      const yl = letter.height * (3 / 6) - badgePt.height / 2;

      doc.rect(xl, yh, badgePt.width, badgePt.height).stroke();
      //doc.text('left', xl + 10, yh + 10);
      doc.rect(xr, yh, badgePt.width, badgePt.height).stroke();
      //doc.text('right', xr + 10, yh + 10);

      doc.rect(xl, yl, badgePt.width, badgePt.height).stroke();
      //doc.text('left', xl + 10, yl + 10);
      doc.rect(xr, yl, badgePt.width, badgePt.height).stroke();
      //doc.text('right', xr + 10, yl + 10);

      if (advanced) {
        doc.text(`Include advanced`, 20, secondMark + 20, {
          height: firstMark - 10,
          width: letter.width - 20,
        });
      }

      doc.text(`Notes:\n${notes}`, 20, secondMark + 50, {
        height: firstMark - 10,
        width: letter.width - 20,
      });

      /* PAGE 2 */
      doc.addPage({ size });

      const pageOffset = letter.height / 3; // Place window contents in middle third of page

      if (returnWindow.outline) {
        // outline of envelope return address window
        doc
          .rect(
            returnWindow.left,
            pageOffset + returnWindow.top,
            returnWindow.width,
            returnWindow.height
          )
          .dash(5, { space: 10 })
          .stroke();
      }

      // address (lines up with envelope window)
      doc.text(
        returnAddressParts.join("\n"),
        returnWindow.left + margin.left,
        pageOffset + returnWindow.top + margin.top,
        { width: returnWindow.width, height: returnWindow.height }
      );

      if (addressWindow.outline) {
        // outline of envelope address window
        doc
          .rect(
            addressWindow.left,
            pageOffset + addressWindow.top,
            addressWindow.width,
            addressWindow.height
          )
          .dash(5, { space: 10 })
          .stroke();
      }

      // address (lines up with envelope window)
      const addressParts = [];
      addressParts.push(name);
      addressParts.push(address1);
      if (address2.length > 0) {
        addressParts.push(address2);
      }
      addressParts.push(`${city}, ${region} ${postalcode}`);
      addressParts.push(country);

      doc.text(
        addressParts.join("\n"),
        addressWindow.left + margin.left,
        pageOffset + addressWindow.top + margin.top,
        {
          width: addressWindow.width,
          height: addressWindow.height,
        }
      );

      doc.end();
    });
  }

  async printAddress(request) {
    const { email } = request;

    const pdf = await this.composePdf(request);

    const addressRequest = {
      "operation-attributes-tag": {
        "requesting-user-name": email,
        "job-name": "address",
        "document-format": "application/pdf",
      },
      "job-attributes-tag": {
        sides: "two-sided-long-edge",
      },
      data: pdf,
    };

    console.log("addressRequest", addressRequest);
    if (NODE_ENV === "development") {
      console.log("Skipping print in development");
      return;
    }
    try {
      const addressResult = await this.print("Print-Job", addressRequest);
      // console.log('addressRequest', await addressResult);
    } catch (e) {
      console.error("LaserPrinter Print-Job error", e);
    }
  }
}

module.exports = LaserPrinter;
