# Sampler Backend

#### Service

1. `sudo cp sampler.service /lib/systemd/system/`
2. `sudo chmod 644 /lib/systemd/system/sampler.service`
3. `sudo systemctl daemon-reload`
4. `sudo systemctl enable sampler.service`
5. `sudo systemctl start sampler.service`

`journalctl --since today -f -u sampler.service`
