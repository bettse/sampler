require("dotenv").config();

const SocketHookClient = require("./SocketHookClient");
const LaserPrinter = require("./LaserPrinter");

const { NOTIFICATION_SHA } = process.env;

const laserPrinter = new LaserPrinter();

async function sleep(ms) {
  return new Promise((resolve, reject) => setTimeout(resolve, ms));
}

async function samplerRequest(request) {
  console.log("--------------------");
  await sleep(2000); // A little time for sockethook to get setup

  try {
    await laserPrinter.printAddress(request);
  } catch {
    console.log('fuck');
  }
}

async function main() {
  const requestHookClient = new SocketHookClient(
    NOTIFICATION_SHA,
    samplerRequest
  );
  console.log("Waiing for messages from SocketHook");

  return ""
}

main().then(console.log);
