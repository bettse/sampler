const util = require("util");
const fs = require("fs");
const { createHash } = require("crypto");
const cookie = require('cookie')
const fetch = require("node-fetch");
const Lob = require("lob");
const jsonwebtoken = require('jsonwebtoken');

const {
  NETLIFY_DEV = false,
  LOB_API_KEY,
  EMAIL_REP_KEY,
  NOTIFICATION_URL,
} = process.env;

const MAX_SIZE_BYTES = 1 * 1024 * 1024;
const EMAIL_REP = "https://emailrep.io";
const request_prefix = "requests";
const badge_prefix = "badges";
const gallery = "gallery";
const validVariants = [
  "primary",
  "secondary",
  "success",
  "danger",
  "warning",
  "info",
  "light",
  "dark"
];

const success = {
  statusCode: 200,
  body: ""
};

const failure = {
  statusCode: 401,
  body: JSON.stringify({
    status: "There was an error processing your request",
    statusVariant: "danger",
  })
};

const { usVerifications } = Lob(LOB_API_KEY);
const verify = util.promisify(usVerifications.verify.bind(usVerifications));

async function addAddressValid(data) {
  const { name, address1, address2, city, region, postalcode, country } = data;
  // Can't verify non-us with this API
  if (['', 'USA', 'UNITED STATES'].includes(country.toUpperCase())) {
    try {
      const result = await verify({
        recipient: name,
        primary_line: address1,
        secondary_line: address2,
        city,
        state: region,
        zip_code: postalcode
      });
      const { deliverability, lob_confidence_score } = result;
      const { score, level } = lob_confidence_score;
      data['usVerifications'] = {deliverability, score, level}
    } catch (e) {
      console.log("address verification issue:", e);
    }
  }
}

//https://github.com/sublime-security/emailrep.io
async function addEmailReputation(data) {
  const { email } = data;
  try {
    const response = await fetch(`${EMAIL_REP}/${email}`, {
      headers: {
        Key: EMAIL_REP_KEY,
        "Content-Type": "application/json",
        "User-Agent": "sampler.ericbetts.dev"
      }
    });
    const result = await response.json();
    if (response.ok) {
      const { reputation, suspicious } = result;
      data['emailReputation'] = {reputation, suspicious}
    } else {
      const { status, reason } = result;
      console.log("email rep error", status, result);
    }
  } catch (e) {
    console.log("error validating email", e);
  }
}

async function notifyBackend(data) {
  if (NETLIFY_DEV) {
    console.log("Skip notification in dev");
    return;
  }
  const response = await fetch(NOTIFICATION_URL, {
    method: "POST",
    body: JSON.stringify(data),
  });
  console.log("POSTed to sockethook", response.ok);
}

async function requestBadge(payload) {
  const { country, email } = payload;
  try {
    // Avoid burning rate limit during development
    if (NETLIFY_DEV) {
      console.log("skip address validation");
    } else {
      //await addAddressValid(payload);
      //await addEmailReputation(payload);
    }

    await notifyBackend(payload);

    return {
      status: "queued",
      statusVariant: "info",
    }
  } catch (e) {
    console.error(e);
    throw e;
  }
}

exports.handler = async function(event, context) {
  const {body: json} = event;
  const body = JSON.parse(json);
  const {payload} = body;
  console.log(payload);
  const {data} = payload;

  try {
    const body = await requestBadge(data);
    return {
      statusCode: 200,
      body: JSON.stringify(body),
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 422,
      body: JSON.stringify({
        status: e,
        statusVariant: "danger"
      })
    };
  }
  return failure;
};
